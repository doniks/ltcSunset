cmake_minimum_required(VERSION 2.8)

project(ltcMelody)
add_definitions(-DTESTING)
add_executable(${PROJECT_NAME} "main.cpp" "melody.cpp")

