#ifdef TESTING
// define this when running through regular desktop gcc
// undefine it when running through loge-to-code
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#define OUTPUT "output"
#define pinMode(led, mode) printf("pinMode(%d, %s)\n", led, mode)
#define analogWrite(led, brightness) printf("analogWrite(%d, %d)\n", led, brightness);
//#define analogWrite(led, brightness) do {} while (0)
#define delay(msec) {struct timespec req = {0, msec * 1000}; struct timespec rem = {0,0}; nanosleep(&req, &rem);}
#endif

#define sun1 3
#define sun2 2
#define sun3 0
#define star 5

void fade(int led, int start, int end, int step, int pause)
{
    if( start <= end )
    {
        for(int i = start; i <= end; i += step)
        {
            analogWrite(led, i);
            delay(pause);
        }
    }else{
        for(int i = start; i >= end; i -= step)
        {
            analogWrite(led, i);
            delay(pause);
        }
    }
}

#define slow 20 // regular performance speed
//#define slow 10 // faster for development
//#define slow 5 // really fast for developemnt
#define fast 1

#define max1 200 // 255
#define max2 120 // 200
#define max3 90 // 120


void setup()
{
    pinMode(sun1, OUTPUT);
    pinMode(sun2, OUTPUT);
    pinMode(sun3, OUTPUT);
    pinMode(star, OUTPUT);

    delay(fast);

    analogWrite(sun1, 0);
    analogWrite(sun2, 0);
    analogWrite(sun3, 0);
    analogWrite(star, 0);
    delay(fast);
    analogWrite(sun1, 0);
    analogWrite(sun2, 0);
    analogWrite(sun3, 0);
    analogWrite(star, 0);
    delay(fast);
}

#define run

void loop()
{

#ifdef run
    fade(sun1, 0, max1, 1, slow);
    fade(sun1, max1, 0, 1, slow);
    fade(sun2, 0, max2, 1, slow);
    fade(sun2, max2, 0, 1, slow);
    fade(sun3, 0, max3, 1, slow);
    fade(sun3, max3, 0, 1, slow);

    delay(100 * slow);
    analogWrite(star, 255);
    delay(10*slow);
    analogWrite(star, 0);



    delay(2000);
#else
  on(sun1);
  on(sun2);
  on(sun3);
  on(star);
#endif
}

